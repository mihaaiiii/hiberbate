package edu.sdr.dc.model;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.BatchSize;

import javax.persistence.*;

@Data
@Entity
@Table (name = "book")
public class Book {

    @Id
    @GeneratedValue (strategy = GenerationType.IDENTITY)

    @Column (name = "id", nullable = false)
    private Long id;

    @Column (name = "title")
    String title;
    String author;
    int pages;

    public Book() {
    }

    public Book(String title, String author, int pages) {
        this.title = title;
        this.author = author;
        this.pages = pages;
    }
}
