package edu.sdr.dc.model;

import lombok.Getter;

import javax.persistence.*;

@Getter
@Entity
public class Rate {

    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    private Integer id;

    @ManyToOne
    private Currency currency;

    private String rate;

    private String date;
    private Integer multiplier;

    @Embedded
    private EntityInfo entityInfo;

    public Rate(){}

    public Rate(String date, Currency currency, String rate, Integer multiplier) {
        this.currency = currency;
        this.rate = rate;
        this.date = date;
        this.multiplier = multiplier;
    }

    public Rate(String date, Currency currency, String rate) {
        this.currency = currency;
        this.rate = rate;
        this.date = date;
        this.multiplier = 1;
    }

    public Rate(Integer id, String date, Currency currency, String rate, Integer multiplier) {
        this.id = id;
        this.currency = currency;
        this.rate = rate;
        this.date = date;
        this.multiplier = multiplier;
    }

    @Override
    public String toString() {
        return "Rate{" +
                "id=" + id +
                ", currency=" + currency +
                ", rate='" + rate + '\'' +
                ", date='" + date + '\'' +
                ", multiplier=" + multiplier +
                ", entityInfo=" + entityInfo +
                '}';
    }
}
