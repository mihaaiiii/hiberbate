package edu.sdr.dc.model;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.util.Date;

@Embeddable
@AllArgsConstructor
@NoArgsConstructor
public class EntityInfo {

    private String description;
    private Date createDate;

    @Override
    public String toString() {
        return "EntityInfo{" +
                "description='" + description + '\'' +
                ", createDate=" + createDate +
                '}';
    }
}
