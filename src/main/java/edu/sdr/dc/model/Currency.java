package edu.sdr.dc.model;

import lombok.Data;
import lombok.Generated;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;

@Entity
@Data
public class Currency {

    @Id
    @GeneratedValue (strategy = GenerationType.IDENTITY)
    private Long id;

    private String name;

    @Embedded
    private EntityInfo entityInfo;
    @OneToMany (mappedBy ="rate", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private List<Rate> rates;

    public Currency() {
    }

    public Currency(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Currency{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", entityInfo=" + entityInfo +
                ", rates=" + rates +
                '}';
    }
}
//to do:
//
//        1. JDBC de implementat toate metodele din interfata repozitory aplicind sql native si mapari;
//        o interafta JDBC Repository inc are sa pastrati Repository.
//
//        - implementati  in clase de repozitorii pentru fiecare tip de clasa din model.
//        - creati nivelul de servicii
//
//        2. Hibernate; creati interafta HibernateRepository - metode comune pentru toate clasele
//        Pentru fiecare clasa creati cite un repozitoriu, in care se implemnteaza  (crud + citeva methode cu parametri) folosind
//        entityManar si HQL .
//
//        - implementati  in clase de repozitorii pentru fiecare tip de clasa din model.
//        - creati nivelul de servicii
//
//        3. MainHibernate / JDBCMain manipulati cu clasele care ati creat.
