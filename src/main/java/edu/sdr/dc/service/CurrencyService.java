package edu.sdr.dc.service;

import edu.sdr.dc.model.Currency;
import edu.sdr.dc.model.EntityInfo;
import edu.sdr.dc.repository.CurrencyRepository;

import java.util.Date;

public class CurrencyService {

    private CurrencyRepository currencyRepository;

    public CurrencyService(CurrencyRepository currencyRepository) {
        this.currencyRepository = currencyRepository;
    }

    public Currency create(Currency currency) {

        if (currency.getEntityInfo() == null) {
            currency.setEntityInfo(new EntityInfo("None", new Date()));
        }

        currency.setName(currency.getName().toUpperCase());
        currencyRepository.save(currency);

        return currency;
    }

}
