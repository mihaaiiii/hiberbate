package edu.sdr.dc.repository;

public interface HibernateRepository<T> {

    T save(T t);

    T delete(T t);

    T update(T t);

    T findById(Long id);
}
