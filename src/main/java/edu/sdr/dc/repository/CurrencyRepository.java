package edu.sdr.dc.repository;

import edu.sdr.dc.model.Currency;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import java.util.List;

public class CurrencyRepository implements HibernateRepository<Currency> {

    private final EntityManager entityManager;

    public CurrencyRepository(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    public List<Currency> selectAllCurrenciesWithNoDescription() {
        return entityManager.createQuery("from Currency where Currency.entityInfo.description is null").getResultList();
    }

    public List<Currency> findByNameOrDescription(String name, String description) {

        return entityManager.createQuery("FROM Currency currency where currency.name LIKE CONCAT('%', :name, '%') OR currency.entityInfo.description LIKE CONCAT('%', :description, '%')")
                .setParameter("name", name)
                .setParameter("description", description)
                .getResultList();
    }

    public List<Currency> findByName(String name) {

        return entityManager.createQuery("FROM Currency currency where currency.name LIKE CONCAT('%', :name, '%')")
                .setParameter("name", name)
                .getResultList();
    }

    public List<Currency> findByDescription(String description) {

        return entityManager.createQuery("FROM Currency currency where currency.entityInfo.description LIKE CONCAT('%', :description, '%')")
                .setParameter("description", description)
                .getResultList();
    }

    @Override
    public Currency save(Currency currency) {

        EntityTransaction entityTransaction = entityManager.getTransaction();
        if (!entityTransaction.isActive()) {
            entityTransaction.begin();
        }
        entityManager.persist(currency);
        entityTransaction.commit();

        return currency;
    }

    @Override

    public Currency delete(Currency currency) {
        return null;
    }

    @Override
    public Currency update(Currency currency) {
        return null;
    }

    @Override
    public Currency findById(Long id) {
        return entityManager.find(Currency.class, id);
    }
}
