package edu.sdr.dc.repository;

import edu.sdr.dc.model.Currency;
import edu.sdr.dc.model.Rate;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.LinkedList;
import java.util.List;

public class RateRepository implements Repository<Rate> {

    private final EntityManager entityManager;

    public RateRepository(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    public int count() {
        return entityManager.createQuery("FROM Rate rate").getResultList().size();
    }

    public BigInteger countByRateUsingNativeQuery() {
        return (BigInteger) entityManager.createNativeQuery("select sum(1) from rate").getSingleResult();
    }

    public Long countByRate() {
        return (Long) entityManager.createQuery("select sum(1) from Rate").getSingleResult();
    }

    public List<Rate> findAllRateNativeQuery() {
        return entityManager.createNativeQuery("select * from rate").getResultList();
    }

    public Rate save(Rate rate) {
        EntityTransaction entityTransaction = entityManager.getTransaction();
        if (!entityTransaction.isActive()) {
            entityTransaction.begin();
        }
        entityManager.persist(rate);
        entityTransaction.commit();
        return rate;
    }

    public List<Rate> findAllByDate(Connection connection, String date) {
        List<Rate> rateList = new LinkedList<>();

        try (Statement readItemsStatement = connection.createStatement()) {
            String readItemsQuery = "SELECT id, currency, rate, date, multiplier " +
                    " FROM rate " +
                    " WHERE date = '" + date + "' ";
            ResultSet rs = readItemsStatement.executeQuery(readItemsQuery);
            while (rs.next()) {
                int id = rs.getInt("id");
                String currency = rs.getString("currency");
                String rate = rs.getString("rate");
                String dateFromTable = rs.getString("date");
                Integer multiplier = rs.getInt("multiplier");

                rateList.add(new Rate(id, dateFromTable, new Currency(), rate, multiplier));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return rateList;
    }

    @Override
    public Rate findById(Integer id) {

        return entityManager.find(Rate.class, id);
    }

    public Rate findByIdAsVIAHQL(Integer id) {

        return (Rate) entityManager.createQuery("FROM Rate rate WHERE rate.id =:id").setParameter("id", id).getSingleResult();
    }

    @Override
    public Rate updateById(Integer id, String rate, String date) {
        return null;
    }

    @Override
    public boolean deleteById(Integer id) {
        return false;
    }

    @Override
    public Rate save(Integer id, String currency, String rate, String date, Integer multiplier) {
        return null;
    }

    @Override
    public void createTable() {
    }

}
