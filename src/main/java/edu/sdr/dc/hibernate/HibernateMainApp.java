package edu.sdr.dc.hibernate;

import edu.sdr.dc.model.Book;
import edu.sdr.dc.model.Currency;
import edu.sdr.dc.model.Rate;
import edu.sdr.dc.repository.BookRepository;
import edu.sdr.dc.repository.CurrencyRepository;
import edu.sdr.dc.repository.RateRepository;
import edu.sdr.dc.service.CurrencyService;
import lombok.extern.slf4j.Slf4j;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import java.util.List;
import java.util.Optional;

@Slf4j
public class HibernateMainApp {

    public static void main(String[] args) throws InterruptedException {

//        Book book = new Book("no name", "author", 2000);
//
//        org.hibernate.SessionFactory sessionFactory = SessionFactoryMaker.getFactory();
//
//        try (Session session = sessionFactory.openSession()) {
//            Transaction tx = session.beginTransaction();
//
//            session.save(book);
//
//            tx.commit();
//        } catch (Exception ex) {
//            ex.printStackTrace();
//        }
//
//        final Session session = sessionFactory.openSession();
//        final BookRepository bookRepository = new BookRepository(session.getSession());
//
//        List<Book> bookList = bookRepository.findAll();
//        System.out.println("Initial number of books is: " + bookList.size());
//
//        Book book2 = bookRepository.save(new Book("no name", "author2", 2002));
//
//        Book book3 = bookRepository.save(new Book("no name", "author3", 2022));
//
//        bookList = bookRepository.findAll();
//        System.out.println("Current number of books is: " + bookList.size());
//
//        Optional<Book> bookFindById = bookRepository.findById(2L);
//        if (bookFindById.isPresent()) {
//            System.out.println("found the book: " + bookFindById.get());
//        } else {
//            System.out.println("Error!");
//        }
//
//        bookRepository.remove(book2);
//
//        bookList = bookRepository.findAll();
//        System.out.println("Current number of books is: " + bookList.size());

        SessionFactory sessionFactory = SessionFactoryMaker.getFactory();
        CurrencyRepository currencyRepository = new CurrencyRepository(sessionFactory.openSession());
        CurrencyService currencyService = new CurrencyService(currencyRepository);
        RateRepository rateRepository = new RateRepository(sessionFactory.openSession());
//
//        Currency currency = new Currency("Ron");
//        currencyService.create(currency);
//        Rate rate1Ron = new Rate("2023-03-19", currency, "1");
//        Rate rate2Ron = new Rate("2022-03-19", currency, "2");
//        Rate rate3Ron = new Rate("2021-03-19", currency, "4");
//        Rate rate4Ron = new Rate("2023-01-10", currency, "2");
//
//        rateRepository.save(rate1Ron);
//        rateRepository.save(rate2Ron);
//        rateRepository.save(rate3Ron);
//        rateRepository.save(rate4Ron);
//
//        System.out.println("count by hiberante: " + rateRepository.count());
//        System.out.println("count of rate by native query: " + rateRepository.findAllRateNativeQuery().size());
//        System.out.println("count of rate by native query: " + rateRepository.countByRateUsingNativeQuery());
//        System.out.println("sum: " + rateRepository.countByRate());

        for (Currency c : currencyRepository.findByDescription("aaa")) {
            System.out.println(c);
        }
        System.out.println("xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx");

        for (Currency c : currencyRepository.findByNameOrDescription("bbb", "aaa")) {
            System.out.println(c);
        }
        System.out.println("xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx");

        Rate rate = rateRepository.findById(3);
        System.out.println(rate + "\n");

        Rate rate1 = rateRepository.findByIdAsVIAHQL(3);
        System.out.println(rate1);
        System.out.println("+--------------------------------------------------------+\n");

        for (Rate r : currencyRepository.findById(8L).getRates()) {
            System.out.println(r);
        }

        System.out.println("+--------------------------------------------------------+\n");

        currencyRepository.findById(1L).getRates().forEach(System.out::println);

        Currency cr = currencyRepository.findById(8L);
        System.out.println(cr);
        System.out.println("+--------------------------------------------------------+\n");
        for (Rate r : cr.getRates()) {
            System.out.println(r);
        }

    }


}
